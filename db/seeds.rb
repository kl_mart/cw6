# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

admin_user = User.create(name: 'Admin', email: 'admin@example.com',
                         password: 'adminadmin',
                         password_confirmation: 'adminadmin', admin: true)

simple_user = User.create(name: 'test', email: 'test@example.com',
                         password: 'testtest',
                         password_confirmation: 'testtest')

users = []

3.times do
  name = Faker::Name.name
  users.push User.create(name: Faker::Name.name, email: Faker::Internet.email(name),
                         password: '12345678', password_confirmation: '12345678')
end

10.times do
   Picture.create(title: Faker::Name.name,
                  img_picture: Faker::Avatar.image,
                  user: users.sample
  )
  end