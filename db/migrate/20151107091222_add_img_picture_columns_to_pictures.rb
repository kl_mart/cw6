class AddImgPictureColumnsToPictures < ActiveRecord::Migration
  def up
    add_attachment :pictures, :img_picture
  end

  def down
    remove_attachment :pictures, :img_picture
  end
end
