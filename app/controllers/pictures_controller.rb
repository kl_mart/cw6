class PicturesController < ApplicationController

  before_action :correct_user, only: [:edit, :update, :destroy]


  def index
    @pictures = Picture.order(created_at: :desc)
  end

  def show
    @picture = Picture.find(params[:id])
  end

  def show_user_pictures
    @user = User.find(params[:id])
    @pictures = @user.pictures.order(created_at: :desc)
  end

  def new
    @picture = Picture.new
  end

  def create
    @picture = current_user.pictures.build(picture_params)
    if @picture.save
      flash[:success] = 'Your picture has been created successfully'
      redirect_to root_url
    else
      render 'new'
    end
  end


  def edit
    @picture = Picture.find(params[:id])
  end

  def update
    @picture = Picture.find(params[:id])

    if @picture.update(picture_params)

      redirect_to pictures_path
    else
      render 'edit'
    end
  end

  def destroy
    Picture.destroy(params[:id])

    redirect_to pictures_path
  end

  private

  def picture_params
    params.require(:picture).permit(:title, :img_picture )
  end

  def correct_user
    @picture = Picture.find(params[:id])
    if @picture.user != current_user       #unless @notice.user == current_user || current_user.admin?
      flash[:danger] = 'This is not your notice.'
      redirect_to root_url
    end
  end

end

