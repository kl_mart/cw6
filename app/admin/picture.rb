ActiveAdmin.register Picture do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end

  permit_params  :title,  :img_picture


  form do |f|
    f.inputs do
      f.input :title
      f.input :img_picture, :as => :file, :hint => image_tag(f.object.img_picture.url(:thumb))
    end
    f.actions
  end


  index do
    selectable_column
    id_column
    column :img_picture do |picture|
      image_tag picture.img_picture.url(:thumb)
    end
    column :title do |picture|
      link_to picture.title, admin_picture_path(picture)
    end
    actions
  end

  show do
    attributes_table do
      row :img_picture do |picture|
        image_tag picture.img_picture.url(:medium)
      end
      row :title
    end
    active_admin_comments
  end


end
