class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
          :rememberable, :trackable, :validatable

  validates :name, presence: true
  validates :email, presence: true
  validates :password, presence: true

  has_many :pictures
  has_many :gallery_pictures
has_many :items
end
