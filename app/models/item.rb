class Item < ActiveRecord::Base
  has_attached_file :item_picture, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :item_picture, content_type: /\Aimage\/.*\Z/
  belongs_to :user
end
